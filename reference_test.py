def reference(is_first_clock, y_prev):

    if is_first_clock:
        u_ref = 0.0
    else:
        u_ref = 1.0

    uc_curr = u_ref - y_prev
    return uc_curr
