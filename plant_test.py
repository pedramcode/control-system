def plant (yc_curr, xp_curr):
     delta_t = 0.01
     q=(1/30)
     w=q*delta_t
     A = 0.999666666666667
     B = 0.01
     C = 1.0
     D = 0.0

     u_curr = yc_curr
     xp_nex = A * xp_curr + B * delta_t * u_curr
     y_curr = xp_curr
     return y_curr, xp_nex


